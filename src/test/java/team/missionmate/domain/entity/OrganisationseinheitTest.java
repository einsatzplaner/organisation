package team.missionmate.domain.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import team.missionmate.domain.valueobject.Adresse;
import team.missionmate.domain.valueobject.Land;
import team.missionmate.domain.valueobject.Stadt;

/**
 * Created by konrad.eichstaedt@gmx.de on 17.06.17.
 */
public class OrganisationseinheitTest {

  @Test
  public void createTest() {

    Organisationseinheit organisationseinheit = new Organisationseinheit("Wache Brandenburg",OrganisationTyp.Feuerwehr,
        new Adresse("Magdeburger Strasse","112",new Stadt("14714","Brandenburg"),new Land("Deutschland")));

    assertNotNull(organisationseinheit.getId());

    assertNotNull(organisationseinheit.getEinsatzmittel());

    assertNotNull(organisationseinheit.getAdresse());

    assertEquals(organisationseinheit.getName(),"Wache Brandenburg");

    assertEquals(organisationseinheit.getTyp(),OrganisationTyp.Feuerwehr);
  }

}
