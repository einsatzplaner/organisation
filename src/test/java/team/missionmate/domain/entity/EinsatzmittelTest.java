package team.missionmate.domain.entity;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

/**
 * Created by konrad.eichstaedt@gmx.de on 17.06.17.
 */

public class EinsatzmittelTest {

  @Test
  public void createEinsatzmittelTest() {

    Einsatzmittel einsatzmittel = new Einsatzmittel("FL BRB 11/33-01","FL BRB 11/33-01","1");

    assertNotNull(einsatzmittel.getId());

    assertNull(einsatzmittel.getEinsatzStart());

    assertNull(einsatzmittel.getEinsatzEnde());

    assertNull(einsatzmittel.getPosition());

    assertEquals(einsatzmittel.getName(),"FL BRB 11/33-01");
  }

  @Test
  public void bewegenTest() {

    Einsatzmittel einsatzmittel = new Einsatzmittel("FL BRB 11/33-01","FL BRB 11/33-01","1");

    einsatzmittel.positionÄndern(new GeoJsonPoint(52.34,12.123));

    assertEquals(einsatzmittel.getPosition(),new GeoJsonPoint(52.34,12.123));
  }

}
