package team.missionmate.domain.valueobject;

public class TaktischesZeichenTyp {
  
  private String id;
  
  private String link;
  
  private String name;
  
  private String info;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String titel) {
    this.name = titel;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }
  
}
