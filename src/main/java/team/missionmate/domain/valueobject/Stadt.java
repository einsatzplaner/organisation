package team.missionmate.domain.valueobject;

public class Stadt {
  
  private String postleitzahl;
  
  private String name;
  
  protected Stadt() {
    super();
  }
  
  public Stadt(String postleitzahl, String name)
  {
    this.postleitzahl = postleitzahl;
    this.name = name;
  }

  public String getPostleitzahl() {
    return postleitzahl;
  }

  public String getName() {
    return name;
  }

}
