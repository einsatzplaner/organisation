package team.missionmate.domain.valueobject;

public class Adresse {

  private String strasse;
  
  private String hausnummer;
  
  private Stadt ort;
  
  private Land land;
  
  protected Adresse() {
    super();
  }
  
  public Adresse(String strasse, String hausnummer, Stadt ort, Land land)
  {
    this.strasse = strasse;
    this.hausnummer = hausnummer;
    this.ort = ort;
    this.land = land;
  }

  public String getStrasse() {
    return strasse;
  }

  public String getHausnummer() {
    return hausnummer;
  }

  public Stadt getOrt() {
    return ort;
  }

  public Land getLand() {
    return land;
  }
  
}
