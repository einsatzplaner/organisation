package team.missionmate.domain.valueobject;

public class Land {
  
  private String name;
  
  protected Land() {
    super();
  }
  
  public Land(String name)
  {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
