package team.missionmate.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import team.missionmate.domain.entity.Einsatz;

/**
 * Created by konrad.eichstaedt@gmx.de on 29.06.17.
 */

@Component
public class EinsatzService {

  @Autowired
  private KafkaTemplate<String, Einsatz> template;

  public void updateEinsatz(Einsatz einsatz)
  {

  }

}
