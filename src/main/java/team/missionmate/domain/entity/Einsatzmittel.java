package team.missionmate.domain.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;

/**
 * Created by konrad.eichstaedt@gmx.de on 17.03.17.
 */

public class Einsatzmittel {

  private String id;

  private LocalDateTime einsatzStart;

  private LocalDateTime einsatzEnde;

  private String name;

  private String funkrufname;

  private String taktischeZeichenID;

  private GeoJsonPoint position;

  public Einsatzmittel(String name, String funkrufname, String taktischeZeichenID) {
    this.id = UUID.randomUUID().toString();
    this.name = name;
    this.funkrufname = funkrufname;
    this.taktischeZeichenID = taktischeZeichenID;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Einsatzmittel that = (Einsatzmittel) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(name, that.name) &&
        Objects.equals(funkrufname, that.funkrufname) &&
        Objects.equals(taktischeZeichenID, that.taktischeZeichenID);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, funkrufname, taktischeZeichenID);
  }

  public void positionÄndern(GeoJsonPoint point) {
    this.position = point;
  }

  public void startEinsatz(GeoJsonPoint point) {
    this.einsatzStart = LocalDateTime.now();
  }

  public void beendenEinsatz() {
    this.einsatzEnde = LocalDateTime.now();
  }

  public String getId() {
    return id;
  }

  public LocalDateTime getEinsatzStart() {
    return einsatzStart;
  }

  public LocalDateTime getEinsatzEnde() {
    return einsatzEnde;
  }

  public String getName() {
    return name;
  }

  public String getFunkrufname() {
    return funkrufname;
  }

  public String getTaktischeZeichenID() {
    return taktischeZeichenID;
  }

  public GeoJsonPoint getPosition() {
    return position;
  }
}
