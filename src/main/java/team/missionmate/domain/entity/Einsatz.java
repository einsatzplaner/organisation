package team.missionmate.domain.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by konrad.eichstaedt@gmx.de on 29.06.17.
 */

public class Einsatz {

  private String id;

  private String name;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  private List<Einsatzmittel> mittel;

  public Einsatz(String name) {
    this.id = UUID.randomUUID().toString();
    this.startDate = LocalDateTime.now();
    this.name = name;
    this.mittel = new ArrayList<>();
  }

  public Einsatz(String name, List<Einsatzmittel> mittel) {
    this.id = UUID.randomUUID().toString();
    this.startDate = LocalDateTime.now();
    this.name = name;
    this.mittel = mittel;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public LocalDateTime getStartDate() {
    return startDate;
  }

  public LocalDateTime getEndDate() {
    return endDate;
  }

  public List<Einsatzmittel> getMittel() {
    return mittel;
  }

  @Override
  public String toString() {
    return "Einsatz{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", startDate=" + startDate +
        ", endDate=" + endDate +
        ", mittel=" + mittel +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Einsatz einsatz = (Einsatz) o;
    return Objects.equals(id, einsatz.id) &&
        Objects.equals(name, einsatz.name) &&
        Objects.equals(startDate, einsatz.startDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, startDate);
  }
}
