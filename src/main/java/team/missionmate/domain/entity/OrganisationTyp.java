package team.missionmate.domain.entity;

/**
 * Created by konrad.eichstaedt@gmx.de on 17.03.17.
 */
public enum OrganisationTyp {

  Feuerwehr, Polizei, THW, Bundeswehr

}
