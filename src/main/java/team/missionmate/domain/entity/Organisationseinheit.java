package team.missionmate.domain.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import team.missionmate.domain.valueobject.Adresse;

/**
 * Created by konrad.eichstaedt@gmx.de on 17.03.17.
 */

public class Organisationseinheit {

  private String id;

  private String name;

  private OrganisationTyp typ;

  private Set<Einsatzmittel> einsatzmittel;

  private Adresse adresse;

  public Organisationseinheit(String name, OrganisationTyp typ,
      Adresse adresse) {

    this.id = UUID.randomUUID().toString();
    this.name = name;
    this.typ = typ;
    this.adresse = adresse;
    this.einsatzmittel = new HashSet<>();
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public OrganisationTyp getTyp() {
    return typ;
  }

  public Set<Einsatzmittel> getEinsatzmittel() {
    return einsatzmittel;
  }

  public Adresse getAdresse() {
    return adresse;
  }

  @Override
  public String toString() {
    return "Organisationseinheit{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", typ=" + typ +
        ", einsatzmittel=" + einsatzmittel +
        ", adresse=" + adresse +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Organisationseinheit that = (Organisationseinheit) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(name, that.name) &&
        typ == that.typ &&
        Objects.equals(adresse, that.adresse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, typ, adresse);
  }
}
