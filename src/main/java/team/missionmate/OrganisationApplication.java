package team.missionmate;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import team.missionmate.domain.entity.Einsatz;

@SpringBootApplication
@EnableEurekaClient
public class OrganisationApplication {

	@Bean
	public ProducerFactory<String, Einsatz> producerFactory() {
		return new DefaultKafkaProducerFactory<String, Einsatz>(producerConfigs());
	}

	@Bean
	public Map<String, Object> producerConfigs() {
		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

		return props;
	}

	@Bean
	public KafkaTemplate<String, Einsatz> kafkaTemplate() {
		return new KafkaTemplate<String, Einsatz>(producerFactory());
	}


	public static void main(String[] args) {
		SpringApplication.run(OrganisationApplication.class, args);
	}
}
