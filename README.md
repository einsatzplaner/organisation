# Service for Organisation and Einsatzkräfte #

This service should offer a REST Interface to handle Organisation and Einsatzkräfte. The service should be used to create for instance a Wehr 
with all "Kräfte und Mittel"

The root aggregate should be a organisation which could be of different type like

* Wehr
* Polizeiwache
* Bundeswehrkaserne
* THW